[README.TXT ENGLISH]
IMPORTANT
Sound files have to be in WAV PCM 16 bit format.

Best place for you files is 'audio' folder in root folder of program.

If program doesn't play your sound files, try to run it as Administrator.

If program doesn't work, well.. go to BIOS :)

[README.TXT POLISH]
WAŻNE
Pliki dźwiękowe muszą być w formacie WAV PCM 16 bit.

Najlepiej wrzucać pliki do folderu 'audio' (znajduje się w mejscu instlacji).

Jeśli program nie odtwarza dźwiękó i wywala błąd to uruchom program, jako administrator.

Ewentulnie wejdź do BIOSu :)
