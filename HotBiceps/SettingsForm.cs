﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MetroFramework;
using MetroFramework.Properties;
using MetroFramework.Native;
using MetroFramework.Localization;
using MetroFramework.Interfaces;
using MetroFramework.Forms;
using MetroFramework.Fonts;
using MetroFramework.Drawing;
using MetroFramework.Controls;
using MetroFramework.Components;
using MetroFramework.Animation;

using HotKeysClassLibrary;
using System.Xml;


namespace HotBiceps
{
    public partial class SettingsForm : MetroForm
    {

        HotKeys_Serialization hkcl = new HotKeys_Serialization();
        public SettingsForm()
        {
            InitializeComponent();

            LoadItemsAndValue(metroComboBox1);
            LoadItemsAndValue(metroComboBox2);
            LoadItemsAndValue(metroComboBox3);
            LoadItemsAndValue(metroComboBox4);
            LoadItemsAndValue(metroComboBox5);
            LoadItemsAndValue(metroComboBox6);
            LoadItemsAndValue(metroComboBox7);
            LoadItemsAndValue(metroComboBox8);
            LoadItemsAndValue(metroComboBox9);
            LoadItemsAndValue(metroComboBox10);
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            metroComboBox1.SelectedText = hkcl.LoadNamesOfKeys("XMLFILE/HotKey_0");
            metroComboBox2.SelectedText = hkcl.LoadNamesOfKeys("XMLFILE/HotKey_1");
            metroComboBox3.SelectedText = hkcl.LoadNamesOfKeys("XMLFILE/HotKey_2");
            metroComboBox4.SelectedText = hkcl.LoadNamesOfKeys("XMLFILE/HotKey_3");
            metroComboBox5.SelectedText = hkcl.LoadNamesOfKeys("XMLFILE/HotKey_4");
            metroComboBox6.SelectedText = hkcl.LoadNamesOfKeys("XMLFILE/HotKey_5");
            metroComboBox7.SelectedText = hkcl.LoadNamesOfKeys("XMLFILE/HotKey_6");
            metroComboBox8.SelectedText = hkcl.LoadNamesOfKeys("XMLFILE/HotKey_7");
            metroComboBox9.SelectedText = hkcl.LoadNamesOfKeys("XMLFILE/HotKey_8");
            metroComboBox10.SelectedText = hkcl.LoadNamesOfKeys("XMLFILE/HotKey_9");
        }

        private bool sameHotKeys;

        private void SaveMetroButton_Click(object sender, EventArgs e)
        {
            string[] KyesKey;
            KyesKey = new string[10];
            string[] stringKey;
            stringKey = new string[10];

            ComboBoxItem itm0 = (ComboBoxItem)metroComboBox1.SelectedItem;
            ComboBoxItem itm1 = (ComboBoxItem)metroComboBox2.SelectedItem;
            ComboBoxItem itm2 = (ComboBoxItem)metroComboBox3.SelectedItem;
            ComboBoxItem itm3 = (ComboBoxItem)metroComboBox4.SelectedItem;
            ComboBoxItem itm4 = (ComboBoxItem)metroComboBox5.SelectedItem;
            ComboBoxItem itm5 = (ComboBoxItem)metroComboBox6.SelectedItem;
            ComboBoxItem itm6 = (ComboBoxItem)metroComboBox7.SelectedItem;
            ComboBoxItem itm7 = (ComboBoxItem)metroComboBox8.SelectedItem;
            ComboBoxItem itm8 = (ComboBoxItem)metroComboBox9.SelectedItem;
            ComboBoxItem itm9 = (ComboBoxItem)metroComboBox10.SelectedItem;

            KyesKey[0] = itm0.Value.ToString(); stringKey[0] = itm0.Name;
            KyesKey[1] = itm1.Value.ToString(); stringKey[1] = itm1.Name;
            KyesKey[2] = itm2.Value.ToString(); stringKey[2] = itm2.Name;
            KyesKey[3] = itm3.Value.ToString(); stringKey[3] = itm3.Name;
            KyesKey[4] = itm4.Value.ToString(); stringKey[4] = itm4.Name;
            KyesKey[5] = itm5.Value.ToString(); stringKey[5] = itm5.Name;
            KyesKey[6] = itm6.Value.ToString(); stringKey[6] = itm6.Name;
            KyesKey[7] = itm7.Value.ToString(); stringKey[7] = itm7.Name;
            KyesKey[8] = itm8.Value.ToString(); stringKey[8] = itm8.Name;
            KyesKey[9] = itm9.Value.ToString(); stringKey[9] = itm9.Name;
           
            for ( int i = 1; i <= 9; i++ )
            {
                if (KyesKey[i] == KyesKey[0])
                {
                    MessageBox.Show(this, "You can't assign hotkey multiple times");
                    sameHotKeys = true;
                }
                else
                    sameHotKeys = false;
            }

            if (sameHotKeys == false)
            {
                HotKeys_Serialization hkcl = new HotKeys_Serialization();
                hkcl.SaveHotKeys(KyesKey);
                hkcl.SaveNamesOfHotKeys(stringKey);
            }
            MetroMessageBox.Show( this, "Your settings has been saved." );
            
            this.Close();
            MainForm mf = new MainForm();
            mf.Show();
        }

        private void CancelMetroButton_Click(object sender, EventArgs e)
        {
            this.Close();
            MainForm mf = new MainForm();
            mf.Show();
        }

        private void LoadItemsAndValue( ComboBox cb )
        {
            cb.Items.Add(new ComboBoxItem("Q", Keys.Q));
            cb.Items.Add(new ComboBoxItem("W", Keys.W));
            cb.Items.Add(new ComboBoxItem("E", Keys.E));
            cb.Items.Add(new ComboBoxItem("R", Keys.R));
            cb.Items.Add(new ComboBoxItem("T", Keys.T));
            cb.Items.Add(new ComboBoxItem("Y", Keys.Y));
            cb.Items.Add(new ComboBoxItem("U", Keys.U));
            cb.Items.Add(new ComboBoxItem("I", Keys.I));
            cb.Items.Add(new ComboBoxItem("O", Keys.O));
            cb.Items.Add(new ComboBoxItem("P", Keys.P));
            cb.Items.Add(new ComboBoxItem("A", Keys.A));
            cb.Items.Add(new ComboBoxItem("S", Keys.S));
            cb.Items.Add(new ComboBoxItem("D", Keys.D));
            cb.Items.Add(new ComboBoxItem("F", Keys.F));
            cb.Items.Add(new ComboBoxItem("G", Keys.G));
            cb.Items.Add(new ComboBoxItem("H", Keys.H));
            cb.Items.Add(new ComboBoxItem("J", Keys.J));
            cb.Items.Add(new ComboBoxItem("K", Keys.K));
            cb.Items.Add(new ComboBoxItem("L", Keys.L));
            cb.Items.Add(new ComboBoxItem("Z", Keys.Z));
            cb.Items.Add(new ComboBoxItem("X", Keys.X));
            cb.Items.Add(new ComboBoxItem("C", Keys.C));
            cb.Items.Add(new ComboBoxItem("V", Keys.V));
            cb.Items.Add(new ComboBoxItem("B", Keys.B));
            cb.Items.Add(new ComboBoxItem("N", Keys.N));
            cb.Items.Add(new ComboBoxItem("M", Keys.M));

            cb.Items.Add(new ComboBoxItem("0", Keys.D0));
            cb.Items.Add(new ComboBoxItem("1", Keys.D1));
            cb.Items.Add(new ComboBoxItem("2", Keys.D2));
            cb.Items.Add(new ComboBoxItem("3", Keys.D3));
            cb.Items.Add(new ComboBoxItem("4", Keys.D4));
            cb.Items.Add(new ComboBoxItem("5", Keys.D5));
            cb.Items.Add(new ComboBoxItem("6", Keys.D6));
            cb.Items.Add(new ComboBoxItem("7", Keys.D7));
            cb.Items.Add(new ComboBoxItem("8", Keys.D8));
            cb.Items.Add(new ComboBoxItem("9", Keys.D9));

            cb.Items.Add(new ComboBoxItem("NumPad0", Keys.NumPad0));
            cb.Items.Add(new ComboBoxItem("NumPad1", Keys.NumPad1));
            cb.Items.Add(new ComboBoxItem("NumPad2", Keys.NumPad2));
            cb.Items.Add(new ComboBoxItem("NumPad3", Keys.NumPad3));
            cb.Items.Add(new ComboBoxItem("NumPad4", Keys.NumPad4));
            cb.Items.Add(new ComboBoxItem("NumPad5", Keys.NumPad5));
            cb.Items.Add(new ComboBoxItem("NumPad6", Keys.NumPad6));
            cb.Items.Add(new ComboBoxItem("NumPad7", Keys.NumPad7));
            cb.Items.Add(new ComboBoxItem("NumPad8", Keys.NumPad8));
            cb.Items.Add(new ComboBoxItem("NumPad9", Keys.NumPad9));

            cb.Items.Add(new ComboBoxItem("F1",  Keys.F1));
            cb.Items.Add(new ComboBoxItem("F2",  Keys.F2));
            cb.Items.Add(new ComboBoxItem("F3",  Keys.F3));
            cb.Items.Add(new ComboBoxItem("F4",  Keys.F4));
            cb.Items.Add(new ComboBoxItem("F5",  Keys.F5));
            cb.Items.Add(new ComboBoxItem("F6",  Keys.F6));
            cb.Items.Add(new ComboBoxItem("F7",  Keys.F7));
            cb.Items.Add(new ComboBoxItem("F8",  Keys.F8));
            cb.Items.Add(new ComboBoxItem("F9",  Keys.F9));
            cb.Items.Add(new ComboBoxItem("F10", Keys.F10));
            cb.Items.Add(new ComboBoxItem("F11", Keys.F11));
            cb.Items.Add(new ComboBoxItem("F12", Keys.F12));

            cb.Items.Add(new ComboBoxItem("BACKSPACE", Keys.Back));
            cb.Items.Add(new ComboBoxItem("SPACE",     Keys.Space));
            cb.Items.Add(new ComboBoxItem("HOME",      Keys.Home));
            cb.Items.Add(new ComboBoxItem("END",       Keys.End));
            cb.Items.Add(new ComboBoxItem("PAGE_UP",   Keys.PageUp));
            cb.Items.Add(new ComboBoxItem("PAGE_DOWN", Keys.PageDown));
        }
    }

    public class ComboBoxItem
    {
        public string Name;
        public Keys Value;
        
        public ComboBoxItem(string name, Keys value)
        {
            Name = name; 
            Value = value;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
