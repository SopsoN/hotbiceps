﻿namespace HotBiceps
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.CancelMetroButton = new MetroFramework.Controls.MetroButton();
            this.SaveMetroButton = new MetroFramework.Controls.MetroButton();
            this.metroComboBox1 = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBox2 = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBox3 = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBox4 = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBox5 = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBox6 = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBox7 = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBox8 = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBox9 = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBox10 = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // CancelMetroButton
            // 
            this.CancelMetroButton.Location = new System.Drawing.Point(24, 262);
            this.CancelMetroButton.Name = "CancelMetroButton";
            this.CancelMetroButton.Size = new System.Drawing.Size(102, 29);
            this.CancelMetroButton.TabIndex = 0;
            this.CancelMetroButton.Text = "Cancel";
            this.CancelMetroButton.UseSelectable = true;
            this.CancelMetroButton.Click += new System.EventHandler(this.CancelMetroButton_Click);
            // 
            // SaveMetroButton
            // 
            this.SaveMetroButton.Location = new System.Drawing.Point(230, 262);
            this.SaveMetroButton.Name = "SaveMetroButton";
            this.SaveMetroButton.Size = new System.Drawing.Size(102, 29);
            this.SaveMetroButton.TabIndex = 1;
            this.SaveMetroButton.Text = "Save";
            this.SaveMetroButton.UseSelectable = true;
            this.SaveMetroButton.Click += new System.EventHandler(this.SaveMetroButton_Click);
            // 
            // metroComboBox1
            // 
            this.metroComboBox1.FormattingEnabled = true;
            this.metroComboBox1.ItemHeight = 23;
            this.metroComboBox1.Location = new System.Drawing.Point(36, 64);
            this.metroComboBox1.Name = "metroComboBox1";
            this.metroComboBox1.Size = new System.Drawing.Size(121, 29);
            this.metroComboBox1.TabIndex = 2;
            this.metroComboBox1.UseSelectable = true;
            // 
            // metroComboBox2
            // 
            this.metroComboBox2.FormattingEnabled = true;
            this.metroComboBox2.ItemHeight = 23;
            this.metroComboBox2.Location = new System.Drawing.Point(36, 99);
            this.metroComboBox2.Name = "metroComboBox2";
            this.metroComboBox2.Size = new System.Drawing.Size(121, 29);
            this.metroComboBox2.TabIndex = 3;
            this.metroComboBox2.UseSelectable = true;
            // 
            // metroComboBox3
            // 
            this.metroComboBox3.FormattingEnabled = true;
            this.metroComboBox3.ItemHeight = 23;
            this.metroComboBox3.Location = new System.Drawing.Point(36, 134);
            this.metroComboBox3.Name = "metroComboBox3";
            this.metroComboBox3.Size = new System.Drawing.Size(121, 29);
            this.metroComboBox3.TabIndex = 4;
            this.metroComboBox3.UseSelectable = true;
            // 
            // metroComboBox4
            // 
            this.metroComboBox4.FormattingEnabled = true;
            this.metroComboBox4.ItemHeight = 23;
            this.metroComboBox4.Location = new System.Drawing.Point(36, 169);
            this.metroComboBox4.Name = "metroComboBox4";
            this.metroComboBox4.Size = new System.Drawing.Size(121, 29);
            this.metroComboBox4.TabIndex = 5;
            this.metroComboBox4.UseSelectable = true;
            // 
            // metroComboBox5
            // 
            this.metroComboBox5.FormattingEnabled = true;
            this.metroComboBox5.ItemHeight = 23;
            this.metroComboBox5.Location = new System.Drawing.Point(36, 204);
            this.metroComboBox5.Name = "metroComboBox5";
            this.metroComboBox5.Size = new System.Drawing.Size(121, 29);
            this.metroComboBox5.TabIndex = 6;
            this.metroComboBox5.UseSelectable = true;
            // 
            // metroComboBox6
            // 
            this.metroComboBox6.ItemHeight = 23;
            this.metroComboBox6.Location = new System.Drawing.Point(211, 64);
            this.metroComboBox6.Name = "metroComboBox6";
            this.metroComboBox6.Size = new System.Drawing.Size(121, 29);
            this.metroComboBox6.TabIndex = 7;
            this.metroComboBox6.UseSelectable = true;
            // 
            // metroComboBox7
            // 
            this.metroComboBox7.ItemHeight = 23;
            this.metroComboBox7.Location = new System.Drawing.Point(211, 99);
            this.metroComboBox7.Name = "metroComboBox7";
            this.metroComboBox7.Size = new System.Drawing.Size(121, 29);
            this.metroComboBox7.TabIndex = 8;
            this.metroComboBox7.UseSelectable = true;
            // 
            // metroComboBox8
            // 
            this.metroComboBox8.FormattingEnabled = true;
            this.metroComboBox8.ItemHeight = 23;
            this.metroComboBox8.Location = new System.Drawing.Point(211, 134);
            this.metroComboBox8.Name = "metroComboBox8";
            this.metroComboBox8.Size = new System.Drawing.Size(121, 29);
            this.metroComboBox8.TabIndex = 9;
            this.metroComboBox8.UseSelectable = true;
            // 
            // metroComboBox9
            // 
            this.metroComboBox9.FormattingEnabled = true;
            this.metroComboBox9.ItemHeight = 23;
            this.metroComboBox9.Location = new System.Drawing.Point(211, 169);
            this.metroComboBox9.Name = "metroComboBox9";
            this.metroComboBox9.Size = new System.Drawing.Size(121, 29);
            this.metroComboBox9.TabIndex = 10;
            this.metroComboBox9.UseSelectable = true;
            // 
            // metroComboBox10
            // 
            this.metroComboBox10.FormattingEnabled = true;
            this.metroComboBox10.ItemHeight = 23;
            this.metroComboBox10.Location = new System.Drawing.Point(211, 204);
            this.metroComboBox10.Name = "metroComboBox10";
            this.metroComboBox10.Size = new System.Drawing.Size(121, 29);
            this.metroComboBox10.TabIndex = 11;
            this.metroComboBox10.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(11, 74);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(19, 19);
            this.metroLabel1.TabIndex = 12;
            this.metroLabel1.Text = "1.";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(11, 109);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(19, 19);
            this.metroLabel2.TabIndex = 13;
            this.metroLabel2.Text = "2.";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(11, 144);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(19, 19);
            this.metroLabel3.TabIndex = 14;
            this.metroLabel3.Text = "3.";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(11, 179);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(19, 19);
            this.metroLabel4.TabIndex = 15;
            this.metroLabel4.Text = "4.";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(11, 214);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(19, 19);
            this.metroLabel5.TabIndex = 16;
            this.metroLabel5.Text = "5.";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(186, 74);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(19, 19);
            this.metroLabel6.TabIndex = 17;
            this.metroLabel6.Text = "6.";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(186, 109);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(19, 19);
            this.metroLabel7.TabIndex = 18;
            this.metroLabel7.Text = "7.";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(186, 144);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(19, 19);
            this.metroLabel8.TabIndex = 19;
            this.metroLabel8.Text = "8.";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(186, 179);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(19, 19);
            this.metroLabel9.TabIndex = 20;
            this.metroLabel9.Text = "9.";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(179, 214);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(26, 19);
            this.metroLabel10.TabIndex = 21;
            this.metroLabel10.Text = "10.";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 312);
            this.Controls.Add(this.metroComboBox10);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroComboBox9);
            this.Controls.Add(this.metroComboBox8);
            this.Controls.Add(this.metroComboBox7);
            this.Controls.Add(this.metroComboBox6);
            this.Controls.Add(this.metroComboBox5);
            this.Controls.Add(this.metroComboBox4);
            this.Controls.Add(this.metroComboBox3);
            this.Controls.Add(this.metroComboBox2);
            this.Controls.Add(this.metroComboBox1);
            this.Controls.Add(this.SaveMetroButton);
            this.Controls.Add(this.CancelMetroButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SettingsForm";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Red;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton CancelMetroButton;
        private MetroFramework.Controls.MetroButton SaveMetroButton;
        private MetroFramework.Controls.MetroComboBox metroComboBox1;
        private MetroFramework.Controls.MetroComboBox metroComboBox2;
        private MetroFramework.Controls.MetroComboBox metroComboBox3;
        private MetroFramework.Controls.MetroComboBox metroComboBox4;
        private MetroFramework.Controls.MetroComboBox metroComboBox5;
        private MetroFramework.Controls.MetroComboBox metroComboBox6;
        private MetroFramework.Controls.MetroComboBox metroComboBox7;
        private MetroFramework.Controls.MetroComboBox metroComboBox8;
        private MetroFramework.Controls.MetroComboBox metroComboBox9;
        private MetroFramework.Controls.MetroComboBox metroComboBox10;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel10;
    }
}