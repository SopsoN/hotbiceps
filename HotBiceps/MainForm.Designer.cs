﻿namespace HotBiceps
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.WlaczMetroButton = new MetroFramework.Controls.MetroButton();
            this.NowyMetroButton = new MetroFramework.Controls.MetroButton();
            this.PomocMetroButton = new MetroFramework.Controls.MetroButton();
            this.SciezkiDataGridView = new System.Windows.Forms.DataGridView();
            this.PlikColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HotKeyDataGridView = new System.Windows.Forms.DataGridView();
            this.HotkeyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.SettingsMetroButton = new MetroFramework.Controls.MetroButton();
            this.SettingsMetroToolTip = new MetroFramework.Components.MetroToolTip();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.NewMetroToolTip = new MetroFramework.Components.MetroToolTip();
            ((System.ComponentModel.ISupportInitialize)(this.SciezkiDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HotKeyDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // WlaczMetroButton
            // 
            this.WlaczMetroButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.WlaczMetroButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.WlaczMetroButton.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.WlaczMetroButton.Location = new System.Drawing.Point(20, 389);
            this.WlaczMetroButton.Name = "WlaczMetroButton";
            this.WlaczMetroButton.Size = new System.Drawing.Size(739, 37);
            this.WlaczMetroButton.Style = MetroFramework.MetroColorStyle.Red;
            this.WlaczMetroButton.TabIndex = 0;
            this.WlaczMetroButton.Text = "Turn on";
            this.WlaczMetroButton.UseSelectable = true;
            this.WlaczMetroButton.Click += new System.EventHandler(this.WlaczMetroButton_Click);
            // 
            // NowyMetroButton
            // 
            this.NowyMetroButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.NowyMetroButton.Location = new System.Drawing.Point(20, 63);
            this.NowyMetroButton.Name = "NowyMetroButton";
            this.NowyMetroButton.Size = new System.Drawing.Size(124, 45);
            this.NowyMetroButton.TabIndex = 1;
            this.NowyMetroButton.Text = "New file";
            this.NewMetroToolTip.SetToolTip(this.NowyMetroButton, "Click to add new WAV 16-bit PCM file.\r\n");
            this.NowyMetroButton.UseSelectable = true;
            this.NowyMetroButton.Click += new System.EventHandler(this.NowyMetroButton_Click);
            // 
            // PomocMetroButton
            // 
            this.PomocMetroButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PomocMetroButton.Location = new System.Drawing.Point(150, 63);
            this.PomocMetroButton.Name = "PomocMetroButton";
            this.PomocMetroButton.Size = new System.Drawing.Size(124, 45);
            this.PomocMetroButton.TabIndex = 2;
            this.PomocMetroButton.Text = "Help";
            this.NewMetroToolTip.SetToolTip(this.PomocMetroButton, "Click to get some information\r\n");
            this.PomocMetroButton.UseSelectable = true;
            this.PomocMetroButton.Click += new System.EventHandler(this.PomocMetroButton_Click);
            // 
            // SciezkiDataGridView
            // 
            this.SciezkiDataGridView.AllowUserToAddRows = false;
            this.SciezkiDataGridView.AllowUserToDeleteRows = false;
            this.SciezkiDataGridView.AllowUserToResizeColumns = false;
            this.SciezkiDataGridView.AllowUserToResizeRows = false;
            this.SciezkiDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SciezkiDataGridView.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.SciezkiDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SciezkiDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SciezkiDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PlikColumn});
            this.SciezkiDataGridView.Location = new System.Drawing.Point(20, 115);
            this.SciezkiDataGridView.MultiSelect = false;
            this.SciezkiDataGridView.Name = "SciezkiDataGridView";
            this.SciezkiDataGridView.ReadOnly = true;
            this.SciezkiDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.SciezkiDataGridView.RowHeadersVisible = false;
            this.SciezkiDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.SciezkiDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.SciezkiDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SciezkiDataGridView.ShowCellErrors = false;
            this.SciezkiDataGridView.ShowCellToolTips = false;
            this.SciezkiDataGridView.ShowEditingIcon = false;
            this.SciezkiDataGridView.Size = new System.Drawing.Size(634, 244);
            this.SciezkiDataGridView.TabIndex = 3;
            this.SciezkiDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SciezkiDataGridView_CellDoubleClick);
            // 
            // PlikColumn
            // 
            this.PlikColumn.HeaderText = "File";
            this.PlikColumn.Name = "PlikColumn";
            this.PlikColumn.ReadOnly = true;
            // 
            // HotKeyDataGridView
            // 
            this.HotKeyDataGridView.AllowUserToAddRows = false;
            this.HotKeyDataGridView.AllowUserToDeleteRows = false;
            this.HotKeyDataGridView.AllowUserToResizeColumns = false;
            this.HotKeyDataGridView.AllowUserToResizeRows = false;
            this.HotKeyDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.HotKeyDataGridView.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.HotKeyDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.HotKeyDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HotKeyDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HotkeyDataGridViewTextBoxColumn});
            this.HotKeyDataGridView.Enabled = false;
            this.HotKeyDataGridView.Location = new System.Drawing.Point(673, 115);
            this.HotKeyDataGridView.MultiSelect = false;
            this.HotKeyDataGridView.Name = "HotKeyDataGridView";
            this.HotKeyDataGridView.ReadOnly = true;
            this.HotKeyDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.HotKeyDataGridView.RowHeadersVisible = false;
            this.HotKeyDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.HotKeyDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.HotKeyDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HotKeyDataGridView.ShowCellErrors = false;
            this.HotKeyDataGridView.ShowCellToolTips = false;
            this.HotKeyDataGridView.ShowEditingIcon = false;
            this.HotKeyDataGridView.Size = new System.Drawing.Size(83, 244);
            this.HotKeyDataGridView.TabIndex = 4;
            // 
            // HotkeyDataGridViewTextBoxColumn
            // 
            this.HotkeyDataGridViewTextBoxColumn.HeaderText = "Hotkey";
            this.HotkeyDataGridViewTextBoxColumn.Name = "HotkeyDataGridViewTextBoxColumn";
            this.HotkeyDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label1.Location = new System.Drawing.Point(621, 429);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "© Michał SopsoN Sobczak";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::HotBiceps.Properties.Resources.hotbiceps2;
            this.pictureBox1.Location = new System.Drawing.Point(330, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(204, 121);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // SettingsMetroButton
            // 
            this.SettingsMetroButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SettingsMetroButton.BackgroundImage")));
            this.SettingsMetroButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.SettingsMetroButton.Location = new System.Drawing.Point(673, 24);
            this.SettingsMetroButton.Name = "SettingsMetroButton";
            this.SettingsMetroButton.Size = new System.Drawing.Size(83, 74);
            this.SettingsMetroButton.TabIndex = 22;
            this.SettingsMetroToolTip.SetToolTip(this.SettingsMetroButton, "Click to set your ten hotkeys.");
            this.SettingsMetroButton.UseSelectable = true;
            this.SettingsMetroButton.Click += new System.EventHandler(this.SettingsMetroButton_Click);
            // 
            // SettingsMetroToolTip
            // 
            this.SettingsMetroToolTip.Style = MetroFramework.MetroColorStyle.Blue;
            this.SettingsMetroToolTip.StyleManager = null;
            this.SettingsMetroToolTip.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.Red;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(23, 125);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(733, 253);
            this.metroPanel1.TabIndex = 23;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // NewMetroToolTip
            // 
            this.NewMetroToolTip.Style = MetroFramework.MetroColorStyle.Blue;
            this.NewMetroToolTip.StyleManager = null;
            this.NewMetroToolTip.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackLocation = MetroFramework.Forms.BackLocation.BottomLeft;
            this.ClientSize = new System.Drawing.Size(779, 446);
            this.Controls.Add(this.SettingsMetroButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.HotKeyDataGridView);
            this.Controls.Add(this.SciezkiDataGridView);
            this.Controls.Add(this.PomocMetroButton);
            this.Controls.Add(this.NowyMetroButton);
            this.Controls.Add(this.WlaczMetroButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.metroPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Red;
            this.Text = "HotBiceps";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.SciezkiDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HotKeyDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton WlaczMetroButton;
        private MetroFramework.Controls.MetroButton NowyMetroButton;
        private MetroFramework.Controls.MetroButton PomocMetroButton;
        private System.Windows.Forms.DataGridView SciezkiDataGridView;
        private System.Windows.Forms.DataGridView HotKeyDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn HotkeyDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlikColumn;
        private MetroFramework.Controls.MetroButton SettingsMetroButton;
        private MetroFramework.Components.MetroToolTip SettingsMetroToolTip;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Components.MetroToolTip NewMetroToolTip;

    }
}

