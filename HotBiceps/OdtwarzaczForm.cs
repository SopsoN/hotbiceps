﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MetroFramework;
using MetroFramework.Properties;
using MetroFramework.Native;
using MetroFramework.Localization;
using MetroFramework.Interfaces;
using MetroFramework.Forms;
using MetroFramework.Fonts;
using MetroFramework.Drawing;
using MetroFramework.Controls;
using MetroFramework.Components;
using MetroFramework.Animation;

using System.Runtime.InteropServices;
using System.Threading;
using System.IO;
using System.Media;
using System.Windows.Input;

using FilePath_serialization;
using HotKeysClassLibrary;

namespace HotBiceps
{
    public partial class OdtwarzaczForm : MetroForm
    {
        private static string[] paths;
        public static Keys[] Keys;

        Thread odtwarzacz = new Thread(new ThreadStart(Start));
        HotKeys_Serialization hkcl = new HotKeys_Serialization();
             
        public OdtwarzaczForm( string[] initpaths )
        {
            InitializeComponent();

            paths = initpaths;

            /* LOADING PREVIOUSLY SAVED HOTKEY VALUES */
            

            /* LOADING PREVIOUSLY SAVED HOTKEY NAMES */
            if (File.Exists("HotKeysNamesSettings.xml"))
            {
                List<string> keys = new List<string>();

                for (int i = 0; i <= 9; i++)
                    keys.Add(hkcl.LoadNamesOfKeys("XMLFILE/HotKey_" + i.ToString()));

                foreach (string str_keys in keys)
                    HotKeyDataGridView.Rows.Add(str_keys);
            }

            string[] pokaz_sciezki = paths;
            List<string> edit_sciezki = new List<string>();

            foreach ( string sciezka in pokaz_sciezki )
            {
                if ( sciezka != "" )
                {
                    string[] lastsciezka = sciezka.Split( '\\' );
                    edit_sciezki.Add( lastsciezka.Last() );
                }
                else
                    edit_sciezki.Add( "" );
            }


        /* ** WYSWIETLENNIE NAZW PLIKOW W TABELI ** */
            pokaz_sciezki = edit_sciezki.ToArray();
            
            for ( int k = 0; k <= 9; k++ )
                PlikDataGridView.Rows.Add(pokaz_sciezki[k]);

            odtwarzacz.Start();
        }

        private void WylaczMetroButton_Click(object sender, EventArgs e)
        {
            SoundPlayer sp = new SoundPlayer();
            sp.Stop();

            KillTheThread();
            this.Close();

            MainForm mf = new MainForm();
            mf.Show();
        }

        private void OdtwarzaczForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            SoundPlayer sp = new SoundPlayer();
            sp.Stop();

            KillTheThread();
            this.Close();

            MainForm mf = new MainForm();
            mf.Show();
        }

        public static void Muzyka(string lokacjaaudio)
        {
            SoundPlayer sp = new SoundPlayer();

                sp.SoundLocation = lokacjaaudio;
                sp.PlaySync();
        }

        public static string PobranyKlawisz;
        [DllImport("user32.dll")]

        public static extern int GetAsyncKeyState(Int32 i);

        public static void Start()
        {
            
            while (true)
            {
                Thread.Sleep(10);
                for (Int32 i = 0; i < 255; i++)
                {
                    int StanKlawisza = GetAsyncKeyState(i);

                    if (StanKlawisza == 1 || StanKlawisza == -32767)
                    {

                        string PobraneKlawisze = Convert.ToString((Keys)i);

                        if (((Keys)i) == Keys[0])
                        {
                            if (paths[0] != "")
                                Muzyka(paths[0]);
                        }
                        else if (((Keys)i) == Keys[1])
                        {
                            if (paths[1] != "")
                                Muzyka(paths[1]);
                        }
                        else if (((Keys)i) == Keys[2])
                        {
                            if (paths[2] != "")
                                Muzyka(paths[2]);
                        }
                        else if (((Keys)i) == Keys[3])
                        {
                            if (paths[3] != "")
                                Muzyka(paths[3]);
                        }
                        else if (((Keys)i) == Keys[4])
                        {
                            if (paths[4] != "")
                                Muzyka(paths[4]);
                        }
                        else if (((Keys)i) == Keys[5])
                        {
                            if (paths[5] != "")
                                Muzyka(paths[5]);
                        }
                        else if (((Keys)i) == Keys[6])
                        {
                            if (paths[6] != "")
                                Muzyka(paths[6]);
                        }
                        else if (((Keys)i) == Keys[7])
                        {
                            if (paths[7] != "")
                                Muzyka(paths[7]);
                        }
                        else if (((Keys)i) == Keys[8])
                        {
                            if (paths[8] != "")
                                Muzyka(paths[8]);
                        }
                        else if (((Keys)i) == Keys[9])
                        {
                            if (paths[9] != "")
                                Muzyka(paths[9]);
                        }

                        //switch ((Keys)i)
                        //{
                        //    case FinalKeys0:
                        //        if (paths[0] != "")
                        //        {
                        //            Muzyka(paths[0]);
                        //        }
                        //    break;

                        //    case FinalKeys1:
                        //        if (paths[1] != "")
                        //        {
                        //            Muzyka(paths[1]);
                        //        }
                        //    break;

                        //    case FinalKeys2:
                        //        if (paths[2] != "")
                        //        {
                        //            Muzyka(paths[2]);
                        //        }
                        //    break;

                        //    case FinalKeys3:
                        //        if (paths[3] != "")
                        //        {
                        //            Muzyka(paths[3]);
                        //        }
                        //    break;

                        //    case FinalKeys4:
                        //        if (paths[4] != "")
                        //        {
                        //            Muzyka(paths[4]);
                        //        }
                        //    break;

                        //    case FinalKeys5:
                        //        if (paths[5] != "")
                        //        {
                        //            Muzyka(paths[5]);
                        //        }
                        //    break;

                        //    case FinalKeys6:
                        //        if (paths[6] != "")
                        //        {
                        //            Muzyka(paths[6]);
                        //        }
                        //    break;

                        //    case FinalKeys7:
                        //        if (paths[7] != "")
                        //        {
                        //            Muzyka(paths[7]);
                        //        }
                        //    break;

                        //    case FinalKeys8:
                        //        if (paths[8] != "")
                        //        {
                        //            Muzyka(paths[8]);
                        //        }
                        //    break;

                        //    case FinalKeys9:
                        //        if ( paths[9] != "" )
                        //        {
                        //            Muzyka(paths[9]);
                        //        }
                        //    break;
                        //}

                    }
                }
            }
        }

        private void OdtwarzaczForm_Shown(object sender, EventArgs e)
        {
            PlikDataGridView.ClearSelection();
            HotKeyDataGridView.ClearSelection();
        }

        public void KillTheThread()
        {
            odtwarzacz.Abort();
        }
    }
}
